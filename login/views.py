from django.shortcuts import render,redirect
from .forms import sign_in, sign_up
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
# Create your views here.
def signin(request):
    if request.method=="POST":
        fillform = sign_in(request.POST)
        if(fillform.is_valid()):
            user_name= fillform.cleaned_data["username"]
            password = fillform.cleaned_data["password"]
            SignIn = authenticate(request, username=user_name,password=password)
            if SignIn is not None :
                login(request,SignIn)
                return redirect('/welcome/')
            else:
                fillform = sign_in()
                return render(request,'login.html',{'form':fillform})
    else:
        fillform=sign_in()
        return render(request,'login.html',{'form':fillform})
def signup(request):
    if request.method =="POST":
        fillform= sign_up(request.POST)
        if (fillform.is_valid()):
            user_name =fillform.cleaned_data["username"]
            email = fillform.cleaned_data["email"]
            password = fillform.cleaned_data["password"]
            first_name1= fillform.cleaned_data["Firstname"]
            last_name1 = fillform.cleaned_data["Lastname"]
            try:
                User.objects.get(username=user_name)
                return redirect("/signup/")
            except:
                x= User.objects.create_user(user_name,email,password)
                x.last_name =last_name1
                x.first_name =first_name1
                x.save()
                return redirect('/')
    else:
        fillform=sign_up()
        return render(request,'register.html',{'form':fillform})
def welcome(request):
    return render(request,'welcome.html')
def signout(request):
    return redirect('/')