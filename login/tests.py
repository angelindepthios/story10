from django.test import TestCase,Client
from django.urls import resolve
from login.views import signin,signup,welcome
from login.forms import sign_in, sign_up
# Create your tests here.
class test_story10(TestCase):
    def test_signin_function(self):
        found = resolve('/')
        self.assertEqual(found.func,signin)
    def test_signup_function(self):
        found = resolve('/signup/')
        self.assertEqual(found.func,signup)
    def test_welcome_function(self):
        found= resolve('/welcome/')
        self.assertEqual(found.func,welcome)
    def test_form(self):
        form1 = sign_in()
        self.assertIn('class="form-control"',form1.as_p())
        self.assertIn('name="username"',form1.as_p())
        self.assertIn('name="password"',form1.as_p())
    def test_form2(self):
        form2 = sign_up()
        self.assertIn('class="form-control"',form2.as_p())
        self.assertIn('name="username"',form2.as_p())
        self.assertIn('name="password"',form2.as_p())
        self.assertIn('name="email"',form2.as_p())
    def test_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response,'login.html')
        response1 = self.client.get('/signup/')
        self.assertTemplateUsed(response1,'register.html')
        response1 = self.client.get('/welcome/')
        self.assertTemplateUsed(response1,'welcome.html')
    def test_render_result(self):
        response_post = Client().post('/',{'username':'test','password':'test'})
        self.assertEqual(response_post.status_code,200)
    def test_register_render(self):
        response_post =Client().post('/signup',{'username':'test','email':'test@gmail.com','password':'test123'})
        self.assertEqual(response_post.status_code,301)