from django.urls import path
from.views import signin,signup,welcome,signout
urlpatterns = [
    path('',signin, name='signin'),
    path('signup/',signup,name='signup'),
    path('welcome/',welcome,name='welcome'),
    path('signout/',signout,name ='signout')
]
